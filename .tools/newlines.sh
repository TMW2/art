#!/bin/bash

source ./.tools/init.sh

aptget_update
aptget_install dos2unix git-core

export LOG1="newlines.log"
SIGNAL=0;

rm ${LOG1}

find -H . -type f -name "*.xml" -exec dos2unix {} \; >${LOG1}
find -H . -type f -name "*.tmx" -exec dos2unix {} \; >>${LOG1}
find -H . -type f -name "*.md" -exec dos2unix {} \; >>${LOG1}
find -H . -type f -name "*.conf" -exec dos2unix {} \; >>${LOG1}

export RES=$(git diff --name-only)
if [[ -n "${RES}" ]]; then
    printf "\n\n-----------------------------------------------------\n\n";
    echo "Wrong new lines detected in several files files:"
    printf "\n\n";
    git diff --name-only
    SIGNAL=1;
fi

exit $SIGNAL;

