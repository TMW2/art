#!/bin/bash

source ./.tools/init.sh

clientdata_init
cd ../client-data ; pwd
aptget_update
aptget_install git-core gcc ca-certificates grep
rm -rf tools
gitclone https://gitlab.com/TMW2 evol-tools.git tools

cd tools/licensecheck
echo "Begun, we are at tools/licensecheck"
ls ../../client-data
echo "Now doing the pipeline"

./clientdata.sh >license.log
check_error $?

export RES=$(cat license.log)
if [[ -n "${RES}" ]]; then
    echo "Detected missing licenses."
    cat license.log
    echo "Estimated total missing licenses:"
    wc -l license.log
    exit 1
fi
